
# Compile CSS

Recursively compiles CSS files from a target folder into a single stylesheet.

Input Parameters:

**target:** Supports either a folder or a load-order file.  
If a folder is provided, compiles all of the .css files into a single .css file.  
If a load-order .css file is provided, builds the output .css file according to the load-order file.

**output:** Supports either a folder or a file. This is where the compiled .css file is saved.

**--watch:** Keeps an eye on the directory for changes and re-compiles the CSS output with each change.

```
> ./compileCSS <target> <output> <--watch>
```