@ECHO off
setlocal EnableDelayedExpansion
ECHO.

:: ===========================================================================
::
:: MAIN PROGRAM
::
:: ===========================================================================

:: ---------------------------------------------------------------------------
::
:: PROCESS PARAMETERS
::
:: ---------------------------------------------------------------------------

:: ---------------------------------------------------------------------------
:: 'help' parameter
:: ---------------------------------------------------------------------------

IF [%1] == [/?] GOTO :help
IF [%1] == [\?] GOTO :help
IF [%1] == [help] GOTO :help
IF [%1] == [-help] GOTO :help
IF [%1] == [--help] GOTO :help

:: ---------------------------------------------------------------------------
:: 'input' and 'output' parameters
:: ---------------------------------------------------------------------------

SET root=%cd%

SET input=%root%\
SET inputFormat="folder"

SET output=%root%\stylesheet.css
SET outputFormat="file"

SET watch=false

IF [%1]==[--watch] (SET watch=true) ELSE IF NOT [%1]==[] (SET input=%~1)
IF [%2]==[--watch] (SET watch=true) ELSE IF NOT [%2]==[] (SET output=%~2)
IF [%3]==[--watch] (SET watch=true)

CALL :SetAbsolutePath "%root%" "%input%" input
CALL :SetAbsolutePath "%root%" "%output%" output

FOR %%i IN ("%input%") DO (SET inputpath=%%~dpi)

:: ensure output is a .css file
IF "x%output:.css=%"=="x%output%" SET output=%output%.css

:: ---------------------------------------------------------------------------
:: begin core loop
:: ---------------------------------------------------------------------------

:: Watch is False so we just issue a build

IF %watch% == false (

	CALL :Build "%input%" "%output%"
	GOTO :end

)

:: Watch is True
:: Iterate through the input folder

FOR /R "%inputpath%" %%i IN (*.*) DO (

	SET file=%%i

	call :FileModTime "!file!" checkModified

	IF !checkModified! gtr !lastModified! (

		SET lastModified=!checkModified!

	)

)

CALL :Build "%input%" "%output%"

:watch

FOR /R "%inputpath%" %%i IN (*.*) DO (

	SET file=%%i

	call :FileModTime "!file!" checkModified

	IF !checkModified! gtr !lastModified! (

		SET lastModified=!checkModified!
		CALL :Build "%input%" "%output%"

	)

)

ping -n 5 127.0.0.1 > nul

GOTO :watch


GOTO :end


:: ---------------------------------------------------------------------------
::
:: SUB-ROUTINES
::
:: ---------------------------------------------------------------------------

:: ---------------------------------------------------------------------------
:: Set Absolute Path
:: ---------------------------------------------------------------------------

:SetAbsolutePath

SET absolutePath=%~1
SET arg=%~2

IF "x%input:.=%"=="x%input%" SET input=%input%\

IF "x%arg::\=%"=="x%arg%" (
	SET "%~3=%absolutePath%\%arg%"
) ELSE (
	SET "%~3=%arg%"
)

GOTO :eof


:: ---------------------------------------------------------------------------
:: Build
:: ---------------------------------------------------------------------------

:Build

SET input=%~1
SET output=%~2

IF EXIST "%output%" BREAK>"%output%"

IF "x%input:.css=%"=="x%input%" (

	CALL :BuildFromFolder "%input%" "%output%"

) ELSE (

	CALL :BuildFromFile "%input%" "%output%"

)

GOTO :eof


:: ---------------------------------------------------------------------------
:: Build From File
:: ---------------------------------------------------------------------------

:BuildFromFile

SET input=%~1
SET output=%~2

:: Parse the source folder from the loadOrder string
FOR %%i IN (%1) DO SET inputDir=%%~pi

:: usebackq - treat string as an object, ie a file location

:: iterate the contents of the loadOrder file
FOR /F "usebackq delims=" %%a IN ("%input%") DO (

	SET line=%%a

	:: only parse lines that have "import="
	IF NOT "x!line:import=!"=="x!line!" (
		SET line="!line:@import =!"
		SET line="!line:@import=!"
		SET line="!line:(=!"
		SET line="!line:)=!"
		SET line="!line:'=!"
		SET line="!line:;=!"
		SET line="!line:"=!"

		:: recursively looks for a matching file in the source directory
		FOR /R "%inputDir%" %%a IN (*) DO (
			IF !line!=="%%~nxa" TYPE "%%a" >> "%output%" & ECHO %%~nxa
		)
	)
)

GOTO :eof


:: ---------------------------------------------------------------------------
:: Build From Folder
:: ---------------------------------------------------------------------------

:BuildFromFolder

SET input=%~1
SET output=%~2

CALL :GetFolderFrom %1 inpath

CD %inpath%

ECHO input: %input%
ECHO input path: %inpath%
ECHO output: %output%

FOR /F "eol=: delims=" %%F IN ('dir /s /b /a-d *.css') DO (

	SET filename=%%F
	IF "x!filename:order=!"=="x!filename!" TYPE "%%F" >> "%output%" & ECHO %%~nxF

)

GOTO :eof


:: ---------------------------------------------------------------------------
:: FileModTime
:: ---------------------------------------------------------------------------

:FileModTime  File  [TimeVar]
::
::  Computes the Unix time (epoch time) for the last modified timestamp for File.
::  The result is an empty string if the file does not exist. Stores the result
::  in TimeVar, or prints the result if TimeVar is not specified.
::
::  Unix time = number of seconds since midnight, January 1, 1970 GMT
::
:: Get full path of file
set "file=%~1"
:: Get last modified time in YYYYMMDDHHMMSS format
set "time="
for /f "skip=1 delims=,. tokens=2" %%A in (
  'wmic datafile where "name='%file:\=\\%'" get lastModified /format:csv 2^>nul'
) do set "ts=%%A"
set "ts=%ts%"
:: Convert time to Unix time (aka epoch time)
if defined ts (
  set /a "yy=10000%ts:~0,4% %% 10000, mm=100%ts:~4,2% %% 100, dd=100%ts:~6,2% %% 100"
  set /a "dd=dd-2472663+1461*(yy+4800+(mm-14)/12)/4+367*(mm-2-(mm-14)/12*12)/12-3*((yy+4900+(mm-14)/12)/100)/4"
  set /a "ss=(((1%ts:~8,2%*60)+1%ts:~10,2%)*60)+1%ts:~12,2%-366100-%ts:~21,1%((1%ts:~22,3%*60)-60000)"
  set /a "ts=ss+dd*86400"
)
:: Return the result
set "%~2=%ts%"
goto :eof


:: ---------------------------------------------------------------------------
:: Get Folder From
:: ---------------------------------------------------------------------------

:GetFolderFrom %path%

SET in=%1

FOR %%i IN (%1) DO (SET %2=%%~dpi)

GOTO :eof


:: ---------------------------------------------------------------------------
:: Unquote
:: ---------------------------------------------------------------------------

:Unquote

	set %1=%~2

GOTO :eof


:: ---------------------------------------------------------------------------
:: Help Request
:: ---------------------------------------------------------------------------

:Help

ECHO.
ECHO Recursively compiles CSS files into a single stylesheet.

ECHO.
ECHO ./compileCSS <target>
ECHO ./compileCSS <target> <output>

ECHO.
ECHO Target: Supports either a folder or a load-order file. If a folder is provided, compiles all of the .css files into a single .css file. If a load-order .css file is provided, builds the output .css file according to the load-order file.
ECHO Output: Supports either a folder or a file. This is where the compiled .css file is saved.

ECHO.
PAUSE

ECHO.
ECHO Example:
ECHO compileCSS.bat "My CSS Snippets"

ECHO.
ECHO / My CSS Snippets
ECHO 	/ 2
ECHO 		- 2A.css
ECHO 	/ 1
ECHO 		/ 1
ECHO 			- 11A.css
ECHO 		- 1A.css
ECHO 		- 1B.css 
ECHO 	- A.css

ECHO.
ECHO Would compile your CSS in the order of:
ECHO A.css
ECHO 1A.css
ECHO 1B.css
ECHO 11A.css
ECHO 2A.css

ECHO.
PAUSE 

ECHO.
ECHO Example:
ECHO compileCSS.bat "My CSS Snippets/loadOrder.css"

ECHO.
ECHO / My CSS Snippets
ECHO 	/ Pages
ECHO 		- Article.css
ECHO 		- Home.css
ECHO 	/ Layouts
ECHO 		- General.css
ECHO 	- extra.css
ECHO 	- loadOrder.css

ECHO.
ECHO LoadOrder.css Contents:
ECHO 	@import('general');
ECHO 	@import('home');
ECHO 	@import('article');
ECHO 	@import('extra');

ECHO.
ECHO Would compile your CSS in the order of:
ECHO General.css
ECHO Home.css
ECHO Article.css
ECHO Extra.css


GOTO :end

:: ---------------------------------------------------------------------------
:: The end is nigh
:: ---------------------------------------------------------------------------

:end

CD %root%